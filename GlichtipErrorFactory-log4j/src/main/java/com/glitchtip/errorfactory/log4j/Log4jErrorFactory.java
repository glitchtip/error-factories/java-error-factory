package com.glitchtip.errorfactory.log4j;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4jErrorFactory {
    private static final Logger log = LogManager.getLogger();

    public static void run() {
        logException();
    }

    private static void logException() {
        log.log(Level.ERROR, "Dummy logged error",
                new RuntimeException("Error"));
    }
}
