package com.glitchtip.errorfactory.legacy;

import com.glitchtip.errorfactory.log4j.JavaUtilLoggingErrorFactory;
import com.glitchtip.errorfactory.log4j.Log4jErrorFactory;

import io.sentry.Sentry;

public class GlitchTipErrorFactory {
    public static void main(String[] args) {
        JavaUtilLoggingErrorFactory.run();
        Log4jErrorFactory.run();

        // Gracefully terminate
        Sentry.close();
    }
}
