package com.glitchtip.errorfactory.legacy;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class RecordHTTPRequests {
    public static void main(String[] args) throws Exception {
        try (ServerSocket serverSocket = new ServerSocket(
                8000,
                0,
                InetAddress.getByName("127.0.0.1"))) {
            System.out.println(
                    "Listening on " + serverSocket.getLocalSocketAddress());
            while (true) {
                Socket socket = serverSocket.accept();
                Thread t = new Thread(() -> {
                    try (Socket s = socket) {
                        readFrom(s);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                t.setDaemon(true);
                t.start();
            }
        }
    }

    private static void readFrom(Socket socket) throws IOException {
        System.err.println("Receive connection " + socket);
        socket.setSoTimeout(0);

        byte[] buf = new byte[4 * 2048];
        int n = socket.getInputStream().read(buf);
        if (n == -1) {
            return;
        }
        System.out.write(buf, 0, n);
    }
}
