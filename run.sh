#!/bin/sh
echo "DSN=$1"

mvn clean install
(cd GlichtipErrorFactory-legacy/ && mvn package exec:java -Dsentry.dsn="$1")
(cd GlichtipErrorFactory-modern/ && mvn package exec:java -Dsentry.dsn="$1")
