FROM alpine
RUN apk add --no-cache maven openjdk8
COPY . /project
WORKDIR /project
CMD [ "./run.sh" ]
