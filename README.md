# Collection of java error factories

## Usage

0. Either ensure maven and a jdk is installed, or use `docker build .` to build a container.
0. Run `./run.sh <Your DSN>` (Or in docker `docker run someHash ./run.sh <Your DSN>`)

## Included tests

Currently both a [legacy][0] and a [modern][1] log4j2-based generator are included. Both the log4j- and the standard java logging API are used to generate errors.

[0]: https://docs.sentry.io/platforms/java/guides/log4j2/legacy/
[1]: https://docs.sentry.io/platforms/java/guides/log4j2/